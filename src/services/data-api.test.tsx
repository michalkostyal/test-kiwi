import { renderHook } from "@testing-library/react-hooks";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import {dataApi} from "./data-api";


test("dataApi performs GET request", async () => {
    const initialValue = [];
    const mock = new MockAdapter(axios);

    const mockResponse = "response";
    const url = "www.mock.com";
    mock.onGet(url).reply(200, mockResponse);

    const { result, waitForNextUpdate } = renderHook(() =>
        dataApi(url, initialValue)
    );

    expect(result.current.data).toEqual([]);
    expect(result.current.isLoading).toBeTruthy();

    await waitForNextUpdate();

    expect(result.current.data).toEqual("response");
    expect(result.current.isLoading).toBeFalsy();

    const url2 = "www.mock.com";
    const mockResponse2 = 2;
    mock.onGet(url2).reply(200, mockResponse2);

    const initialValue2 = "initial value 2";
    const { result: result2, waitForNextUpdate: waitForNextUpdate2 } = renderHook(
        () => dataApi(url2, initialValue2)
    );

    expect(result2.current.data).toEqual("initial value 2");
    expect(result2.current.isLoading).toBeTruthy();

    await waitForNextUpdate2();

    expect(result2.current.data).toEqual(2);
    expect(result2.current.isLoading).toBeFalsy();
});

test("on network error set loading to false and returns inital value", async () => {
    const mock = new MockAdapter(axios);

    const initialValue = [];
    const url = "www.mock.com";

    mock.onGet(url).networkError();

    const { result, waitForNextUpdate } = renderHook(() =>
        dataApi(url, initialValue)
    );

    expect(result.current.data).toEqual([]);
    expect(result.current.isLoading).toBeTruthy();

    await waitForNextUpdate();

    expect(result.current.isLoading).toBeFalsy();
    expect(result.current.data).toEqual([]);
});


