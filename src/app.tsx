import React from 'react';
import EnergyMix from "./pages/EnergyMix/energy-mix";
import "./app.css"

const App: React.FC = () => (
    <div className="main-container">
        <EnergyMix  />
    </div>
)

export default App;
