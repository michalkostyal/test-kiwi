import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import {dataApi} from "../../services/data-api";
import EnergyMix from "./energy-mix";


jest.mock('../../services/data-api');

describe("EnergyMix", () => {
  const data = {data:
        { data:{
            from: "2019-10-29T10:00Z",
            to: "2019-10-29T10:30Z",
            generationmix: [
              { fuel: "coal", perc: 50 }, { fuel: "gas", perc: 10 },
              { fuel: "solar", perc: 30 }, { fuel: "hydro", perc: 10 }]
        }, isLoading: false, isError:false}
  };

  const dataLoading = { isLoading: true, isError:false};
  const dataError = { isLoading: false, isError:true};

  let container = null;
  beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it("matches snapshot", async () => {
    (dataApi as jest.Mock<any>).mockImplementation(() => data);
    await act(async () => {
      render(<EnergyMix />, container);
    });
    expect(container.innerHTML).toMatchSnapshot();
  });


  it("has 4 elements in legend", async () => {
    (dataApi as jest.Mock<any>).mockImplementation(() => data);
    await act(async () => {
      render(<EnergyMix />, container);
    });
    expect(container.querySelectorAll(".donutchart-legend-item").length).toBe(4);
  });

  it("show loading text during loading", async () => {
    (dataApi as jest.Mock<any>).mockImplementation(() => dataLoading);
    await act(async () => {
      render(<EnergyMix />, container);
    });
    expect(container.querySelectorAll(".loading").length).toBe(1);
  });

  it("show error text when error received", async () => {
    (dataApi as jest.Mock<any>).mockImplementation(() => dataError);
    await act(async () => {
      render(<EnergyMix />, container);
    });
    expect(container.querySelectorAll(".error").length).toBe(1);
  });

});
