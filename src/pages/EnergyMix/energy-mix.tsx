import React, { Fragment } from 'react';
import {dataApi} from "../../services/data-api";
import DonutChart from 'react-donut-chart';

const endpoint = 'https://api.carbonintensity.org.uk/generation'

interface EnergyItem {
    fuel: string;
    perc: number;
}

const EnergyMix: React.FC = ()  =>{
    const { data, isLoading, isError } = dataApi(endpoint, []);
    return (
        <Fragment>
            {isError && <div className="error">Something went wrong ...</div>}
            {isLoading && <div className="loading">Loading ...</div> }
            {(!isError && !isLoading && data.data && data.data.generationmix) &&
            <div className="chart-holder">
                <h1>UK energy generation data - updated at {new Date(data.data.from).toLocaleTimeString('en-us')}
                </h1>
                <DonutChart
                    data={ data.data.generationmix.map((item: EnergyItem)=> ({label: item.fuel, value: item.perc})) }
                />
            </div>
            }
        </Fragment>
    )
}

export default EnergyMix
