

## Getting started
* Get the dependencies - `npm install`
* Run the app - `npm start` - it will be available at http://localhost:8080
* Run the tests -  `npm test`
